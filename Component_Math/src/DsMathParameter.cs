﻿using System;

namespace Yamabuki.Component.Math
{
    internal class DsMathParameter
    {
        public const String AutoCastEnabeld = "Yamabuki.Component.Math.AutoCastEnabled";

        public const String AutoCastEnabeld_Enabled = "true";

        public const String AutoCastEnabeld_Disabled = "false";
    }
}
