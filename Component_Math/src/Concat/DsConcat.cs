﻿using System;
using System.Collections.Generic;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsConcat
        : DsAggregateOperator_N_1
    {
        internal override string Title
        {
            get { return "Concat"; }
        }

        internal override String Description
        {
            get
            {
                return "データを連結します。\r\n" +
                    "「入力データ数」には連結するデータの数を指定してください。\r\n" +
                    "例 : 入力データ数が2のとき\r\n" +
                    "入力1 : [1 2 3]\r\n" +
                    "入力2 : [2 3 4]\r\n" +
                    "出力 : [1 2 3 2 3 4]";
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsConcat(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuid,
                TsAppContext.TsCreateFunction,
                TsAppContext.TsConcatFunction);
        }
    }
}
