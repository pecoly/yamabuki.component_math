﻿using System;
using System.Collections.Generic;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsNor
        : DsLogicalOperator_N_1
    {
        internal override string Title
        {
            get { return "否定論理和"; }
        }

        internal override String Description
        {
            get
            {
                return "データの否定論理和を求めます。\r\n" +
                    "配列の値同士の場合は\r\n" +
                    "配列の各要素ごとの否定論理和となります。\r\n" +
                    "配列の値と単体の値の場合は\r\n" +
                    "配列の各要素と単体の値の否定論理和となります。\r\n" +
                    "「入力データ数」が1のときは配列の各要素の否定論理和となります。";
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsLogicalOperator_A(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuid,
                TsAppContext.TsCreateFunction,
                TsAppContext.TsNorFunction);
        }
    }
}
