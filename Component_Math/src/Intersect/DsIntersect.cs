﻿using System;
using System.Collections.Generic;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsIntersect
        : DsAggregateOperator_N_1
    {
        internal override string Title
        {
            get { return "Intersect"; }
        }

        internal override String Description
        {
            get
            {
                return "データの積集合を求めます。\r\n" +
                    "「入力データ数」には積集合を求めるデータの数を指定してください。\r\n" +
                    "例 : 入力データ数が2のとき\r\n" +
                    "入力1 : [1 2 3]\r\n" +
                    "入力2 : [2 3 4]\r\n" +
                    "出力 : [2 3]";
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsIntersect(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuid,
                TsAppContext.TsCreateFunction,
                TsAppContext.TsIntersectFunction);
        }
    }
}
