﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;

namespace Yamabuki.Component.Math
{
    public abstract class DsLogicalOperator_1_1
        : DsSimpleComponent_1_1
    {
        internal Int32 InputDataLength { get; set; }

        internal abstract String Title { get; }

        internal abstract String Description { get; }

        public override void Initialize(IEnumerable<XElement> data)
        {
        }

        public override BaseMessage DoubleClick()
        {
            var presenter = new DsSimpleEditForm_P(this);
            presenter.Show(FormSize.Auto, this.TypeName, this.Description);
            return null;
        }

        protected override void Initialize_1_1()
        {
        }
    }
}
