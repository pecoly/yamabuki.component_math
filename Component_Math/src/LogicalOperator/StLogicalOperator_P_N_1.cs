﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal class StLogicalOperator_P_N_1
        : DsEditForm_P<StLogicalOperator_VI_N_1, DsLogicalOperator_N_1>
    {
        private String title;

        private String description;

        public StLogicalOperator_P_N_1(DsLogicalOperator_N_1 com, String title, String description)
            : base(com)
        {
            this.title = title;
            this.description = description;
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.Title = this.title;
            this.View.Description = this.description;
            this.View.InputDataLength = this.Component.InputDataLength;
        }

        public override void Save()
        {
            this.Component.InputDataLength = this.View.InputDataLength;
        }
    }
}
