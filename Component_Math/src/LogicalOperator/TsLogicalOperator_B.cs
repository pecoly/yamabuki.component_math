﻿using System;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;
using Yamabuki.Task.Simple;

namespace Yamabuki.Component.Math
{
    public class TsLogicalOperator_B
        : TsSimpleTask_1_1<Boolean, Boolean>
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid">入力データストア</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="func">実行関数</param>
        public TsLogicalOperator_B(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid,
            TsLogicalFunction_B func)
            : base(definitionPath, inputDataStoreGuid, outputDataStoreGuid)
        {
            this.Func = func;
        }
        
        internal TsLogicalFunction_B Func { get; private set; }
        
        protected override void ExecuteInternal(
            TsDataList inputDataList,
            TsDataList outputDataList)
        {
            try
            {
                this.Func.Execute(inputDataList, outputDataList);
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    case TsErrorCodeList.TsInvalidDataType:
                        throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }
    }
}
