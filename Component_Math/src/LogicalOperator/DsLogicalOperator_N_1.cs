﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Utility.Cast;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.Math
{
    public abstract class DsLogicalOperator_N_1
        : DsSimpleComponent_N_1
    {
        internal Int32 InputDataLength { get; set; }

        internal abstract String Title { get; }

        internal abstract String Description { get; }

        public override void Initialize(IEnumerable<XElement> data)
        {
            foreach (var e in data)
            {
                if (e.Name == Property.InputDataLength)
                {
                    this.InputDataLength = CastUtils.ToInt32(e.Value, (x) => 2);
                }
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldInputDataLength = this.InputDataLength;
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            var messageCollection = new MessageCollection();

            foreach (var message in this.UpdateDataInputMessage(oldInputDataLength, this.InputDataLength))
            {
                messageCollection.Add(message);
            }

            messageCollection.Add(new UpdateComponentMessage(oldE, this.GetXElement()));

            return messageCollection;
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                if (kv.Key == Property.InputDataLength)
                {
                    this.InputDataLength = CastUtils.ToInt32(kv.Value, (x) => 2);
                }
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.InputDataLength, this.InputDataLength));
            return list;
        }

        internal IEnumerable<BaseMessage> UpdateDataInputMessage(
            Int32 oldInputDataLength, Int32 newInputDataLength)
        {
            var messageList = new List<BaseMessage>();

            if (oldInputDataLength == newInputDataLength)
            {
                return messageList;
            }
            else if (oldInputDataLength < newInputDataLength)
            {
                // 追加されたとき
                for (var i = 0; i < newInputDataLength - oldInputDataLength; i++)
                {
                    messageList.Add(this.AddInputPort(""));
                }
            }
            else if (newInputDataLength < oldInputDataLength)
            {
                // 削除されたとき
                var index = oldInputDataLength - 1;
                for (var i = 0; i < oldInputDataLength - newInputDataLength; i++)
                {
                    messageList.Add(this.RemoveInputPort(index));
                    index--;
                }
            }

            return messageList;
        }

        protected override void Initialize_N_1()
        {
            this.AddInputPort("");
            this.AddInputPort("");
            this.InputDataLength = 2;
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StLogicalOperator_P_N_1(this, this.Title, this.Description))
            using (var view = new StLogicalOperator_V_N_1())
            {
                presenter.View = view;
                return presenter.ShowModal();
            }
        }

        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String InputDataLength = "InputDataLength";
        }
    }
}
