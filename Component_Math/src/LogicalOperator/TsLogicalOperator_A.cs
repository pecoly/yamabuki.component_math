﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;
using Yamabuki.Task.Utility;
using Yamabuki.Utility;

namespace Yamabuki.Component.Math
{
    public class TsLogicalOperator_A
        : TsTask_N_1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuidList">入力データストア一覧</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">実行関数</param>
        public TsLogicalOperator_A(
            String definitionPath,
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsLogicalFunction_A func)
            : base(definitionPath, inputDataStoreGuidList, outputDataStoreGuid)
        {
            this.CreateFunc = createFunc;
            this.Func = func;
        }
        
        internal TsCreateFunction CreateFunc { get; private set; }

        internal TsLogicalFunction_A Func { get; private set; }

        protected override void CheckInputDataList(
            IEnumerable<TsDataList> inputDataListList)
        {
            foreach (var inputDataList in inputDataListList)
            {
                var convertedInputDataList = TsAppContext.AutoCastEnabled ?
                    inputDataList.Convert(typeof(Boolean)) : inputDataList;

                TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(Boolean), convertedInputDataList.Type);
            }
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            outputDataList = this.CreateFunc.Execute(typeof(Boolean));
        }

        protected override void ExecuteInternal(
            IEnumerable<TsDataList> inputDataListList,
            TsDataList outputDataList)
        {
            try
            {
                if (inputDataListList.Count() == 1)
                {
                    this.Func.Execute(inputDataListList.First(), outputDataList);
                }
                else
                {
                    var inputList = inputDataListList.ToList();
                    var inputDataList0 = inputList[0];

                    var convertedInputDataList0 = TsAppContext.AutoCastEnabled ?
                        inputDataList0.Convert(typeof(Boolean)) : inputDataList0;

                    var tmpList1 = this.CreateFunc.Execute(typeof(Boolean));
                    var tmpList2 = this.CreateFunc.Execute(typeof(Boolean));
                    tmpList1.CopyFrom(convertedInputDataList0);

                    for (var i = 1; i < inputList.Count; i++)
                    {
                        var inputDataList = inputList[i];
                        var convertedInputDataList = TsAppContext.AutoCastEnabled ?
                            inputDataList.Convert(typeof(Boolean)) : inputDataList;

                        tmpList2 = this.CreateFunc.Execute(typeof(Boolean));
                        this.Func.Execute(tmpList1, convertedInputDataList, tmpList2);
                        tmpList1 = tmpList2;
                    }

                    outputDataList.CopyFrom(tmpList2);
                }
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    case TsErrorCodeList.TsInvalidDataType:
                        throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }
    }
}
