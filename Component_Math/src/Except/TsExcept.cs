﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Task.Data;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;

namespace Yamabuki.Component.Math
{
    public class TsExcept
        : TsAggregateOperator_N_1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuidList">入力データストア一覧</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">差集合関数</param>
        public TsExcept(
            String definitionPath,
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsAggregateFunction_2_1 func)
            : base(
            definitionPath,
            inputDataStoreGuidList,
            outputDataStoreGuid,
            createFunc,
            func)
        {
        }

        protected override void ExecuteInternal(
            IEnumerable<TsDataList> inputDataListList,
            TsDataList outputDataList)
        {
            var inputList = inputDataListList.ToList();
            var inputDataList0 = inputList[0];
            var tmpList = this.CreateFunc.Execute(inputDataList0.Type);
            tmpList.CopyFrom(inputDataList0);

            for (var i = 1; i < inputList.Count; i++)
            {
                this.Func.Execute(tmpList, inputList[i], tmpList);
            }

            outputDataList.CopyFrom(tmpList);
        }
    }
}
