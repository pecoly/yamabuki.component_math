﻿using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal interface StRelationalOperator_VI
        : DsEditForm_VI
    {
        MdRelationalOperator.ComparationType ComparationType { get; set; }
    }
}
