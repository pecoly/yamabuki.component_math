﻿using System;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;

namespace Yamabuki.Component.Math
{
    public class TsRelationalOperator
        : TsTask_2_1
    {
        private TsFunction_2_1 func;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid0">入力データストア0</param>
        /// <param name="inputDataStoreGuid1">入力データストア1</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="func">比較関数</param>
        public TsRelationalOperator(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid,
            TsFunction_2_1 func)
            : base(
            definitionPath,
            inputDataStoreGuid0,
            inputDataStoreGuid1,
            outputDataStoreGuid)
        {
            this.func = func;
        }

        protected override void CheckInputDataList(
            TsDataList inputDataList0,
            TsDataList inputDataList1)
        {
            var type0 = inputDataList0.Type;
            var type1 = inputDataList1.Type;

            try
            {
                var result = this.func.GetResultType(type0, type1);
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInvalidDataType:
                        throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            var inputDataStore0 = this.GetInputDataStore(Constants.InputPortIndex0);
            var inputDataStore1 = this.GetInputDataStore(Constants.InputPortIndex1);

            var type0 = inputDataStore0.DataList.Type;
            var type1 = inputDataStore1.DataList.Type;

            var result = this.func.GetResultType(type0, type1);
            outputDataList = new TsDataListT<Boolean>();
        }

        protected override void ExecuteInternal(
            TsDataList inputDataList0,
            TsDataList inputDataList1,
            TsDataList outputDataList)
        {
            try
            {
                this.func.Execute(inputDataList0, inputDataList1, outputDataList);
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    case TsErrorCodeList.TsInvalidDataType:
                        throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }
    }
}
