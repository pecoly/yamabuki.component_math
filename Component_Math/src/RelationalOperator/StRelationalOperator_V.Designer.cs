﻿namespace Yamabuki.Component.Math
{
    partial class StRelationalOperator_V
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.descriptionGroupBox = new System.Windows.Forms.GroupBox();
            this.descriptionLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.okButton = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cancelButton = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.comparationTypeComboBox = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.descriptionGroupBox.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comparationTypeComboBox)).BeginInit();
            this.SuspendLayout();
            // 
            // descriptionGroupBox
            // 
            this.descriptionGroupBox.Controls.Add(this.descriptionLabel);
            this.descriptionGroupBox.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.descriptionGroupBox.Location = new System.Drawing.Point(12, 12);
            this.descriptionGroupBox.Name = "descriptionGroupBox";
            this.descriptionGroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.descriptionGroupBox.Size = new System.Drawing.Size(410, 137);
            this.descriptionGroupBox.TabIndex = 20;
            this.descriptionGroupBox.TabStop = false;
            this.descriptionGroupBox.Text = "比較";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Location = new System.Drawing.Point(6, 25);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(258, 84);
            this.descriptionLabel.TabIndex = 3;
            this.descriptionLabel.Values.Text = "数値を比較した結果を真理値で出力します。\r\n例 : 比較演算子が「＜」のとき\r\n入力1 : [1 2 3]\r\n入力2 : [3 2 -4]\r\n出力 : [true" +
    " false false]\r\n";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(12, 155);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.kryptonLabel3.Size = new System.Drawing.Size(78, 20);
            this.kryptonLabel3.TabIndex = 22;
            this.kryptonLabel3.Values.Text = "比較演算子 : ";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 229);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(434, 23);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 23;
            // 
            // statusLabel
            // 
            this.statusLabel.ForeColor = System.Drawing.Color.Red;
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(23, 18);
            this.statusLabel.Text = "---";
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(206, 198);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(105, 28);
            this.okButton.TabIndex = 1;
            this.okButton.Values.Text = "OK";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(317, 198);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(105, 28);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Values.Text = "キャンセル";
            // 
            // comparationTypeComboBox
            // 
            this.comparationTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comparationTypeComboBox.DropDownWidth = 150;
            this.comparationTypeComboBox.Location = new System.Drawing.Point(96, 154);
            this.comparationTypeComboBox.Name = "comparationTypeComboBox";
            this.comparationTypeComboBox.Size = new System.Drawing.Size(150, 21);
            this.comparationTypeComboBox.TabIndex = 24;
            // 
            // StRelationalOperator_V
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(434, 252);
            this.Controls.Add(this.comparationTypeComboBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.descriptionGroupBox);
            this.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "StRelationalOperator_V";
            this.Text = "比較";
            this.descriptionGroupBox.ResumeLayout(false);
            this.descriptionGroupBox.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comparationTypeComboBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox descriptionGroupBox;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel descriptionLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonButton okButton;
        private ComponentFactory.Krypton.Toolkit.KryptonButton cancelButton;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox comparationTypeComboBox;
    }
}