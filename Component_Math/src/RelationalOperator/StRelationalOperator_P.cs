﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal class StRelationalOperator_P
        : DsEditForm_P<StRelationalOperator_VI, DsRelationalOperator>
    {
        public StRelationalOperator_P(DsRelationalOperator com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.ComparationType = this.Component.ComparationType;
        }

        public override void Save()
        {
            this.Component.ComparationType = this.View.ComparationType;
        }
    }
}
