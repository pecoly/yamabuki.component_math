﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.ExceptionEx;
using Yamabuki.Design.Message;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Function.Base;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.Math
{
    public class DsRelationalOperator
        : DsSimpleComponent_2_1
    {
        internal MdRelationalOperator.ComparationType ComparationType { get; set; }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.ComparationType = MdRelationalOperator.ComparationType.Equal;

            if (data != null)
            {
                foreach (var e in data)
                {
                    if (e.Name == Property.ComparationType)
                    {
                        this.ComparationType = MdRelationalOperator.ToComparationType(e.Value);
                    }
                }
            }

            this.InitializeTypeImage(MdRelationalOperator.ToString(this.ComparationType));
        }

        public override BaseMessage DoubleClick()
        {
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            this.InitializeTypeImage(MdRelationalOperator.ToString(this.ComparationType));

            return new UpdateComponentMessage(oldE, this.GetXElement());
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                if (kv.Key == Property.ComparationType)
                {
                    this.ComparationType = MdRelationalOperator.ToComparationType(kv.Value);
                }
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.ComparationType, MdRelationalOperator.ToString(this.ComparationType)));
            return list;
        }

        protected override void Initialize_2_1()
        {
            this.ComparationType = MdRelationalOperator.ComparationType.Equal;
            this.InitializeTypeImage(MdRelationalOperator.ToString(this.ComparationType));
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StRelationalOperator_P(this))
            using (var view = new StRelationalOperator_V())
            {
                presenter.View = view;
                return presenter.ShowModal();
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var createFunc = TsAppContext.TsCreateFunction;
            var addFunc = TsAppContext.TsAddFunction;

            TsFunction_2_1 func = null;
            switch (this.ComparationType)
            {
                case MdRelationalOperator.ComparationType.Equal:
                    func = TsAppContext.TsEqualFunction;
                    break;
                case MdRelationalOperator.ComparationType.NotEqual:
                    func = TsAppContext.TsNotEqualFunction;
                    break;
                case MdRelationalOperator.ComparationType.LessThan:
                    func = TsAppContext.TsLessThanFunction;
                    break;
                case MdRelationalOperator.ComparationType.LessEqual:
                    func = TsAppContext.TsLessEqualFunction;
                    break;
                case MdRelationalOperator.ComparationType.GreaterThan:
                    func = TsAppContext.TsGreaterThanFunction;
                    break;
                case MdRelationalOperator.ComparationType.GreaterEqual:
                    func = TsAppContext.TsGreaterEqualFunction;
                    break;
                default:
                    throw new DsException(this.DefinitionPath, DsErrorCodeList.DsUnknown, "不正な比較型が指定されました。");
            }

            return new TsRelationalOperator(
                this.DefinitionPath,
                inputDataStoreGuid0,
                inputDataStoreGuid1,
                outputDataStoreGuid,
                func);
        }
        
        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String ComparationType = "ComparationType";
        }
    }
}
