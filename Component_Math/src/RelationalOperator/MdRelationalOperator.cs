﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yamabuki.Component.Math
{
    public class MdRelationalOperator
    {
        public enum ComparationType
        {
            /// <summary>＝＝</summary>
            Equal,

            /// <summary>！＝</summary>
            NotEqual,

            /// <summary>＜</summary>
            LessThan,

            /// <summary>＜＝</summary>
            LessEqual,

            /// <summary>＞</summary>
            GreaterThan,

            /// <summary>＞＝</summary>
            GreaterEqual,
        }

        public static String ToString(ComparationType type)
        {
            switch (type)
            {
                case ComparationType.Equal:
                    return "＝＝";
                case ComparationType.NotEqual:
                    return "！＝";
                case ComparationType.LessThan:
                    return "＜";
                case ComparationType.LessEqual:
                    return "＜＝";
                case ComparationType.GreaterThan:
                    return "＞";
                case ComparationType.GreaterEqual:
                    return "＞＝";
                default:
                    return "";
            }
        }

        public static ComparationType ToComparationType(String value)
        {
            switch (value)
            {
                case "＝＝":
                    return ComparationType.Equal;
                case "！＝":
                    return ComparationType.NotEqual;
                case "＜":
                    return ComparationType.LessThan;
                case "＜＝":
                    return ComparationType.LessEqual;
                case "＞":
                    return ComparationType.GreaterThan;
                case "＞＝":
                    return ComparationType.GreaterEqual;
                default:
                    return ComparationType.Equal;
            }
        }
    }
}
