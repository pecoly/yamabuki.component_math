﻿using System;
using System.Data;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    public partial class StRelationalOperator_V
        : DsEditForm_V, StRelationalOperator_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        private DataTable comparationTypeDataTable = new DataTable();

        public StRelationalOperator_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public MdRelationalOperator.ComparationType ComparationType
        {
            get { return (MdRelationalOperator.ComparationType)this.comparationTypeComboBox.SelectedValue; }
            set { this.comparationTypeComboBox.SelectedValue = value; }
        }

        public override void Initialize()
        {
            this.comparationTypeComboBox.Focus();

            this.InitializeComparationTypeComboBox();
        }

        private void InitializeComparationTypeComboBox()
        {
            this.comparationTypeDataTable = this.CreateComparationTypeTable();

            this.comparationTypeComboBox.DataSource = this.comparationTypeDataTable;
            this.comparationTypeComboBox.DisplayMember = ComparationTypeTable.Name;
            this.comparationTypeComboBox.ValueMember = ComparationTypeTable.ComparationType;
        }

        private DataTable CreateComparationTypeTable()
        {
            var comparationTypeTable = new DataTable();

            comparationTypeTable.Columns.Add(ComparationTypeTable.Name, typeof(String));
            comparationTypeTable.Columns.Add(ComparationTypeTable.ComparationType, typeof(MdRelationalOperator.ComparationType));

            comparationTypeTable.Rows.Add(new Object[] { "==", MdRelationalOperator.ComparationType.Equal });
            comparationTypeTable.Rows.Add(new Object[] { "!=", MdRelationalOperator.ComparationType.NotEqual });
            comparationTypeTable.Rows.Add(new Object[] { "<", MdRelationalOperator.ComparationType.LessThan });
            comparationTypeTable.Rows.Add(new Object[] { "<=", MdRelationalOperator.ComparationType.LessEqual });
            comparationTypeTable.Rows.Add(new Object[] { ">", MdRelationalOperator.ComparationType.GreaterThan });
            comparationTypeTable.Rows.Add(new Object[] { ">=", MdRelationalOperator.ComparationType.GreaterEqual });

            return comparationTypeTable;
        }

        public class ComparationTypeTable
        {
            public const String Name = "Name";
            public const String ComparationType = "ComparationType";
        }
    }
}
