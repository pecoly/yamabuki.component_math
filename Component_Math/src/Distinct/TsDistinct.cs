﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Task.Data;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;

namespace Yamabuki.Component.Math
{
    public class TsDistinct
        : TsAggregateOperator_1_1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid">入力データストア</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">一意関数</param>
        public TsDistinct(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsAggregateFunction_1_1 func)
            : base(
            definitionPath,
            inputDataStoreGuid,
            outputDataStoreGuid,
            createFunc,
            func)
        {
        }

        protected override void ExecuteInternal(
            TsDataList inputDataList,
            TsDataList outputDataList)
        {
            this.Func.Execute(inputDataList, outputDataList);
        }
    }
}
