﻿using System;
using System.Collections.Generic;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsDistinct
        : DsAggregateOperator_1_1
    {
        public override String TypeName
        {
            get { return "重複削除"; }
        }

        public override int DefaultWidth
        {
            get { return 84; }
        }

        internal override String Description
        {
            get
            {
                return "データの重複を削除します。\r\n" +
                    "例 : \r\n" +
                    "入力1 : [1 6 2 3 2 4 5 1 2]\r\n" +
                    "出力 : [1 6 2 3 4 5]";
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsDistinct(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid,
                TsAppContext.TsCreateFunction,
                TsAppContext.TsDistinctFunction);
        }
    }
}
