﻿using System;
using System.Collections.Generic;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsNot
        : DsLogicalOperator_1_1
    {
        internal override string Title
        {
            get { return "否定"; }
        }

        internal override String Description
        {
            get
            {
                return "データの否定を求めます。";
            }
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsLogicalOperator_B(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid,
                TsAppContext.TsNotFunction);
        }
    }
}
