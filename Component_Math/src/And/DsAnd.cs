﻿using System;
using System.Collections.Generic;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsAnd
        : DsLogicalOperator_N_1
    {
        internal override string Title
        {
            get { return "論理積"; }
        }

        internal override String Description
        {
            get
            {
                return "データの論理積を求めます。\r\n" +
                    "配列の値同士の場合は\r\n" +
                    "配列の各要素ごとの論理積となります。\r\n" +
                    "配列の値と単体の値の場合は\r\n" +
                    "配列の各要素と単体の値の論理積となります。\r\n" + 
                    "「入力データ数」が1のときは配列の各要素の論理積となります。";
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsLogicalOperator_A(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuid,
                TsAppContext.TsCreateFunction,
                TsAppContext.TsAndFunction);
        }
    }
}
