﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    public partial class StMathOperator_V
        : DsEditForm_V, StMathOperator_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        public StMathOperator_V()
        {
            this.InitializeComponent();
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public String Description
        {
            set { this.descriptionLabel.Text = value; }
        }

        public String Title
        {
            set
            {
                this.Text = value;
                this.descriptionGroupBox.Text = value;
            }
        }

        public Int32 InputDataLength
        {
            get { return Decimal.ToInt32(this.inputDataLengthUpDown.Value); }
            set { this.inputDataLengthUpDown.Value = value; }
        }
    }
}
