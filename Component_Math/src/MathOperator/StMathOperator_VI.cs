﻿using System;
using System.Text;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal interface StMathOperator_VI
        : DsEditForm_VI
    {
        String Title { set; }
        
        String Description { set; }

        Int32 InputDataLength { get; set; }
    }
}
