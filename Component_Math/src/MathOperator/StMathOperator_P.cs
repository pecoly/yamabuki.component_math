﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal class StMathOperator_P
        : DsEditForm_P<StMathOperator_VI, DsMathOperator>
    {
        private String title;

        private String description;

        public StMathOperator_P(DsMathOperator com, String title, String description)
            : base(com)
        {
            this.title = title;
            this.description = description;
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.Title = this.title;
            this.View.Description = this.description;
            this.View.InputDataLength = this.Component.InputDataLength;
        }

        public override void Save()
        {
            this.Component.InputDataLength = this.View.InputDataLength;
        }
    }
}
