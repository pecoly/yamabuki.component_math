﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;

namespace Yamabuki.Component.Math
{
    public class TsMathOperator_N_1
        : TsTask_N_1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuidList">入力データストア一覧</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">実行関数</param>
        public TsMathOperator_N_1(
            String definitionPath,
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsMathFunction func)
            : base(definitionPath, inputDataStoreGuidList, outputDataStoreGuid)
        {
            this.CreateFunc = createFunc;
            this.Func = func;
        }
        
        internal TsCreateFunction CreateFunc { get; private set; }

        internal TsMathFunction Func { get; private set; }

        protected override void CheckInputDataList(
            IEnumerable<TsDataList> inputDataListList)
        {
            foreach (var x in this.InputDataStoreList)
            {
                if (x.DataList.Type != typeof(Int32) && x.DataList.Type != typeof(Single))
                {
                    throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                }
            }
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            var typeList = this.InputDataStoreList.Select(x => x.DataList.Type);
            var resultType = this.Func.GetResultType(typeList);
            outputDataList = this.CreateFunc.Execute(resultType);
        }

        protected override void ExecuteInternal(
            IEnumerable<TsDataList> inputDataListList,
            TsDataList outputDataList)
        {
            this.Func.Execute(inputDataListList, outputDataList);
        }
    }
}
