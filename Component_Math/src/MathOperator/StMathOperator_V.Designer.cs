﻿namespace Yamabuki.Component.Math
{
    partial class StMathOperator_V
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.descriptionGroupBox = new System.Windows.Forms.GroupBox();
            this.descriptionLabel = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.inputDataLengthUpDown = new ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown();
            this.okButton = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cancelButton = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.descriptionGroupBox.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // descriptionGroupBox
            // 
            this.descriptionGroupBox.Controls.Add(this.descriptionLabel);
            this.descriptionGroupBox.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.descriptionGroupBox.Location = new System.Drawing.Point(12, 12);
            this.descriptionGroupBox.Name = "descriptionGroupBox";
            this.descriptionGroupBox.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.descriptionGroupBox.Size = new System.Drawing.Size(410, 137);
            this.descriptionGroupBox.TabIndex = 20;
            this.descriptionGroupBox.TabStop = false;
            this.descriptionGroupBox.Text = "Concat";
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.Location = new System.Drawing.Point(6, 25);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(369, 100);
            this.descriptionLabel.TabIndex = 3;
            this.descriptionLabel.Values.Text = "データを連結します。\r\n「入力データ数」には連結するデータの数を指定してください。\r\n例 : 入力データ数が2のとき\r\n入力1 : [1 2 3]\r\n入力2 : " +
    "[2 3 4]\r\n出力 : [1 2 3 2 3 4]\r\n";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.Location = new System.Drawing.Point(12, 155);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.kryptonLabel3.Size = new System.Drawing.Size(91, 20);
            this.kryptonLabel3.TabIndex = 22;
            this.kryptonLabel3.Values.Text = "入力データ数 : ";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 229);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(434, 23);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 23;
            // 
            // statusLabel
            // 
            this.statusLabel.ForeColor = System.Drawing.Color.Red;
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(23, 18);
            this.statusLabel.Text = "---";
            // 
            // inputDataLengthUpDown
            // 
            this.inputDataLengthUpDown.Location = new System.Drawing.Point(109, 155);
            this.inputDataLengthUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.inputDataLengthUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.inputDataLengthUpDown.Name = "inputDataLengthUpDown";
            this.inputDataLengthUpDown.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.inputDataLengthUpDown.Size = new System.Drawing.Size(105, 22);
            this.inputDataLengthUpDown.TabIndex = 0;
            this.inputDataLengthUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.inputDataLengthUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(206, 198);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(105, 28);
            this.okButton.TabIndex = 1;
            this.okButton.Values.Text = "OK";
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(317, 198);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(105, 28);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Values.Text = "キャンセル";
            // 
            // StMathOperator_V
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(434, 252);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.inputDataLengthUpDown);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.kryptonLabel3);
            this.Controls.Add(this.descriptionGroupBox);
            this.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "StMathOperator_V";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Concat";
            this.descriptionGroupBox.ResumeLayout(false);
            this.descriptionGroupBox.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox descriptionGroupBox;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel descriptionLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonNumericUpDown inputDataLengthUpDown;
        private ComponentFactory.Krypton.Toolkit.KryptonButton okButton;
        private ComponentFactory.Krypton.Toolkit.KryptonButton cancelButton;
    }
}