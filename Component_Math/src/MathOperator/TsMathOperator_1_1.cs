﻿using System;

using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;

namespace Yamabuki.Component.Math
{
    public class TsMathOperator_1_1
        : TsTask_1_1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid">入力データストア</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">実行関数</param>
        public TsMathOperator_1_1(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsMathFunction func)
            : base(definitionPath, inputDataStoreGuid, outputDataStoreGuid)
        {
            this.CreateFunc = createFunc;
            this.Func = func;
        }
        
        internal TsCreateFunction CreateFunc { get; private set; }

        internal TsMathFunction Func { get; private set; }

        protected override void CheckInputDataList(
            TsDataList inputDataList)
        {
            this.Func.GetResultType(inputDataList.Type);
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            var inputDataStore = this.GetInputDataStore(0);
            var resultType = this.Func.GetResultType(inputDataStore.DataList.Type);
            outputDataList = this.CreateFunc.Execute(resultType);
        }

        protected override void ExecuteInternal(
            TsDataList inputDataList,
            TsDataList outputDataList)
        {
            this.Func.Execute(inputDataList, outputDataList);
        }
    }
}
