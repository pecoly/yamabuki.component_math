﻿using System;

using Yamabuki.Task.Data;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;
using Yamabuki.Task.Function.Multiply;

namespace Yamabuki.Component.Math
{
    public class TsMultiply
        : TsArithmeticOperator
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid0">入力データストア0</param>
        /// <param name="inputDataStoreGuid1">入力データストア1</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="multiplyFunc">乗算関数</param>
        public TsMultiply(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsFunction_2_1 multiplyFunc)
            : base(
            definitionPath,
            inputDataStoreGuid0,
            inputDataStoreGuid1,
            outputDataStoreGuid,
            createFunc,
            multiplyFunc)
        {
        }
    }
}
