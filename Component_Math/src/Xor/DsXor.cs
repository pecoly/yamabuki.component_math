﻿using System;
using System.Collections.Generic;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsXor
        : DsLogicalOperator_N_1
    {
        internal override string Title
        {
            get { return "排他的論理和"; }
        }

        internal override String Description
        {
            get
            {
                return "データの排他的論理和を求めます。\r\n" +
                    "配列の値同士の場合は\r\n" +
                    "配列の各要素ごとの排他的論理和となります。\r\n" +
                    "配列の値と単体の値の場合は\r\n" +
                    "配列の各要素と単体の値の排他的論理和となります。\r\n" +
                    "「入力データ数」が1のときは配列の各要素の排他的論理和となります。";
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsLogicalOperator_A(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuid,
                TsAppContext.TsCreateFunction,
                TsAppContext.TsXorFunction);
        }
    }
}
