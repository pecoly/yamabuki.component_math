﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Utility;
using Yamabuki.Utility;

namespace Yamabuki.Component.Math
{
    public class DsMin
        : DsMathOperator
    {
        internal override string Title
        {
            get { return "最小値"; }
        }

        internal override String Description
        {
            get
            {
                return "データの最小値を求めます。\r\n" +
                    "配列の値同士の場合は\r\n" +
                    "配列の各要素ごとの最小値となります。\r\n" +
                    "配列の値と単体の値の場合は\r\n" +
                    "配列の各要素と単体の値の最小値となります。\r\n" +
                    "「入力データ数」が1のときは配列の各要素の最小値となります。";
            }
        }

        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            if (inputDataStoreGuidList.Count() == 1)
            {
                return new TsMathOperator_1_1(
                    this.DefinitionPath,
                    inputDataStoreGuidList.ElementAt(0),
                    outputDataStoreGuid,
                    TsAppContext.TsCreateFunction,
                    TsAppContext.TsMinFunction);
            }
            else
            {
                return new TsMathOperator_N_1(
                    this.DefinitionPath,
                    inputDataStoreGuidList,
                    outputDataStoreGuid,
                    TsAppContext.TsCreateFunction,
                    TsAppContext.TsMinFunction);
            }
        }
    }
}
