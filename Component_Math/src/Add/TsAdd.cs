﻿using System;

using Yamabuki.Task.Data;
using Yamabuki.Task.Function.Add;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;

namespace Yamabuki.Component.Math
{
    public class TsAdd
        : TsArithmeticOperator
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid0">入力データストア0</param>
        /// <param name="inputDataStoreGuid1">入力データストア1</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="addFunc">加算関数</param>
        public TsAdd(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsFunction_2_1 addFunc)
            : base(
            definitionPath,
            inputDataStoreGuid0,
            inputDataStoreGuid1,
            outputDataStoreGuid,
            createFunc,
            addFunc)
        {
        }
    }
}
