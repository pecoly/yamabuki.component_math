﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;

namespace Yamabuki.Component.Math
{
    public class DsAdd
        : DsSimpleComponent_2_1
    {
        private const String Feature = "加算を行います。\r\n" +
                "配列の値同士の場合は\r\n" +
                "配列の各要素ごとの加算結果となります。\r\n" +
                "配列の値と単体の値の場合は\r\n" +
                "配列の各要素と単体の値の加算結果となります。";

        public override String TypeName
        {
            get { return "Add"; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.InitializeTypeImage("＋");
        }

        public override BaseMessage DoubleClick()
        {
            var presenter = new DsSimpleEditForm_P(this);
            presenter.Show(FormSize.Auto, this.TypeName, Feature);
            return null;
        }

        protected override void Initialize_2_1()
        {
            this.InitializeTypeImage("＋");
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var createFunc = TsAppContext.TsCreateFunction;
            var addFunc = TsAppContext.TsAddFunction;

            return new TsAdd(
                this.DefinitionPath,
                inputDataStoreGuid0,
                inputDataStoreGuid1,
                outputDataStoreGuid,
                createFunc,
                addFunc);
        }
    }
}
