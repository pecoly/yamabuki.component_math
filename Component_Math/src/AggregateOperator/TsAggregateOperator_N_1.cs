﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;

namespace Yamabuki.Component.Math
{
    public abstract class TsAggregateOperator_N_1
        : TsTask_N_1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuidList">入力データストア一覧</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">実行関数</param>
        public TsAggregateOperator_N_1(
            String definitionPath,
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsAggregateFunction_2_1 func)
            : base(definitionPath, inputDataStoreGuidList, outputDataStoreGuid)
        {
            this.CreateFunc = createFunc;
            this.Func = func;
        }
        
        internal TsCreateFunction CreateFunc { get; private set; }

        internal TsAggregateFunction_2_1 Func { get; private set; }

        protected override void CheckInputDataList(
            IEnumerable<TsDataList> inputDataListList)
        {
            var listList = inputDataListList.ToList();
            for (var i = 1; i < listList.Count; i++)
            {
                if (listList[i - 1].Type != listList[i].Type)
                {
                    throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                }
            }
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            var inputDataStore0 = this.GetInputDataStore(0);
            outputDataList = this.CreateFunc.Execute(inputDataStore0.DataList.Type);
        }
    }
}
