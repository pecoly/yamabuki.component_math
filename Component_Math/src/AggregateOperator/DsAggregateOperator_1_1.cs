﻿using System;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;

namespace Yamabuki.Component.Math
{
    public abstract class DsAggregateOperator_1_1
        : DsSimpleComponent_1_1
    {
        internal Int32 InputDataLength { get; set; }

        internal abstract String Description { get; }

        public override BaseMessage DoubleClick()
        {
            using (var presenter = new DsSimpleEditForm_P(this))
            {
                presenter.Show(FormSize.Auto, this.TypeName, this.Description);
                return null;
            }
        }

        protected override void Initialize_1_1()
        {
        }
    }
}
