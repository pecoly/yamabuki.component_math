﻿using System;
using System.Collections.Generic;
using System.Linq;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;

namespace Yamabuki.Component.Math
{
    public abstract class TsAggregateOperator_1_1
        : TsTask_1_1
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid">入力データストア</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">実行関数</param>
        public TsAggregateOperator_1_1(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsAggregateFunction_1_1 func)
            : base(definitionPath, inputDataStoreGuid, outputDataStoreGuid)
        {
            this.CreateFunc = createFunc;
            this.Func = func;
        }
        
        internal TsCreateFunction CreateFunc { get; private set; }

        internal TsAggregateFunction_1_1 Func { get; private set; }

        protected override void CheckInputDataList(
            TsDataList inputDataList)
        {
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            var inputDataStore0 = this.GetInputDataStore(0);
            outputDataList = this.CreateFunc.Execute(inputDataStore0.DataList.Type);
        }
    }
}
