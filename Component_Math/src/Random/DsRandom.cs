﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.Message;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Utility.Cast;
using Yamabuki.Window.Core;

namespace Yamabuki.Component.Math
{
    public class DsRandom
        : DsSimpleComponent_N_1
    {
        public override int DefaultWidth
        {
            get { return 72; }
        }

        internal MdRandom.RandomType RandomType { get; set; }

        internal Int32 Length { get; set; }

        internal Int32 MinValue { get; set; }
        
        internal Int32 MaxValue { get; set; }

        public override void Initialize(IEnumerable<XElement> data)
        {
            foreach (var e in data)
            {
                if (e.Name == Property.RandomType)
                {
                    this.RandomType = MdRandom.ToRandomType(e.Value);
                }
                else if (e.Name == Property.Length)
                {
                    this.Length = CastUtils.ToInt32(e.Value, (y) => 1);
                }
                else if (e.Name == Property.MinValue)
                {
                    this.MinValue = CastUtils.ToInt32(e.Value, (y) => 0);
                }
                else if (e.Name == Property.MaxValue)
                {
                    this.MaxValue = CastUtils.ToInt32(e.Value, (y) => 0);
                }
            }
        }

        public override BaseMessage DoubleClick()
        {
            var oldRandomType = this.RandomType;
            var oldE = this.GetXElement();
            var result = this.ShowDialog();

            var isUpdated = result == FormResult.Ok;
            if (!isUpdated)
            {
                return null;
            }

            var messageCollection = new MessageCollection();

            messageCollection.Add(this.UpdateInputMessage(oldRandomType, this.RandomType));
            messageCollection.Add(new UpdateComponentMessage(oldE, this.GetXElement()));

            return messageCollection;
        }

        public override void SetParameter(IDictionary<String, String> paramList)
        {
            foreach (var kv in paramList)
            {
                if (kv.Key == Property.RandomType)
                {
                    this.RandomType = MdRandom.ToRandomType(kv.Value);
                }
                else if (kv.Key == Property.Length)
                {
                    this.Length = CastUtils.ToInt32(kv.Value, (y) => 0);
                }
                else if (kv.Key == Property.MinValue)
                {
                    this.MinValue = CastUtils.ToInt32(kv.Value, (y) => 0);
                }
                else if (kv.Key == Property.MaxValue)
                {
                    this.MaxValue = CastUtils.ToInt32(kv.Value, (y) => 0);
                }
                else
                {
                    Debug.Assert(false);
                }
            }
        }

        public override IEnumerable<XElement> DataToXElement()
        {
            var list = new List<XElement>();
            list.Add(new XElement(Property.RandomType, MdRandom.ToString(this.RandomType)));
            list.Add(new XElement(Property.Length, this.Length));
            list.Add(new XElement(Property.MinValue, this.MinValue));
            list.Add(new XElement(Property.MaxValue, this.MaxValue));
            return list;
        }

        internal BaseMessage UpdateInputMessage(MdRandom.RandomType oldRandomType, MdRandom.RandomType newRandomType)
        {
            if (oldRandomType != MdRandom.RandomType.ExternalMinMax &&
                newRandomType == MdRandom.RandomType.ExternalMinMax)
            {
                var messageCollection = new MessageCollection();
                messageCollection.Add(this.InsertInputPort(0, "最小値"));
                messageCollection.Add(this.InsertInputPort(1, "最大値"));
                return messageCollection;
            }
            else if (oldRandomType == MdRandom.RandomType.ExternalMinMax &&
                newRandomType != MdRandom.RandomType.ExternalMinMax)
            {
                var messageCollection = new MessageCollection();
                messageCollection.Add(this.RemoveInputPort(1));
                messageCollection.Add(this.RemoveInputPort(0));
                return messageCollection;
            }

            return new NullMessage();
        }

        protected override void Initialize_N_1()
        {
            this.RandomType = MdRandom.RandomType.None;
            this.Length = 1;
            this.MinValue = 0;
            this.MaxValue = 10;
        }

        protected virtual FormResult ShowDialog()
        {
            using (var presenter = new StRandom_P(this))
            using (var view = new StRandom_V())
            {
                presenter.View = view;
                return presenter.ShowModal();
            }
        }
        
        protected override TsTask GetTask(
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid)
        {
            return new TsRandom(
                this.DefinitionPath,
                inputDataStoreGuidList,
                outputDataStoreGuid,
                this.RandomType,
                this.Length,
                this.MinValue,
                this.MaxValue);
        }

        /// <summary>プロパティ</summary>
        private struct Property
        {
            internal const String RandomType = "RandomType";
            internal const String Length = "Length";
            internal const String MinValue = "MinValue";
            internal const String MaxValue = "MaxValue";
        }
    }
}
