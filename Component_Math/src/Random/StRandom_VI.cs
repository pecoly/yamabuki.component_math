﻿using System;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal interface StRandom_VI
        : DsEditForm_VI
    {
        MdRandom.RandomType RandomType { get; set; }

        String Length { get; set; }

        String MinValue { get; set; }

        String MaxValue { get; set; }

        void ClearError();

        void SetLengthError(String value);

        void SetMinValueError(String value);

        void SetMaxValueError(String value);
    }
}
