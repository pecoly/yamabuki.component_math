﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using MathNet.Numerics.Random;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Utility;

namespace Yamabuki.Component.Math
{
    internal class TsRandom
        : TsTask_N_1
    {
        private MdRandom.RandomType randomType;

        private Int32 length;

        private Int32 minValue;

        private Int32 maxValue;

        public TsRandom(
            String definitionPath,
            IEnumerable<TsDataStoreGuid> inputDataStoreGuidList,
            TsDataStoreGuid outputDataStoreGuid,
            MdRandom.RandomType randomType,
            Int32 length,
            Int32 minValue,
            Int32 maxValue)
            : base(
            definitionPath,
            inputDataStoreGuidList,
            outputDataStoreGuid)
        {
            this.randomType = randomType;
            this.length = length;
            this.minValue = minValue;
            this.maxValue = maxValue;
        }

        protected override void CheckInputDataList(IEnumerable<TsDataList> inputDataListList)
        {
            if (this.randomType != MdRandom.RandomType.ExternalMinMax)
            {
                return;
            }

            Debug.Assert(inputDataListList.Count() == 2);

            var convertedInputDataList0 = TsAppContext.AutoCastEnabled ?
                inputDataListList.ElementAt(0).Convert(typeof(Int32)) : inputDataListList.ElementAt(0);
            var convertedInputDataList1 = TsAppContext.AutoCastEnabled ?
                inputDataListList.ElementAt(1).Convert(typeof(Int32)) : inputDataListList.ElementAt(1);

            TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(Int32), convertedInputDataList0.Type);
            TsDataTypeCheckUtils.Check(this.DefinitionPath, typeof(Int32), convertedInputDataList1.Type);
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            outputDataList = new TsDataListT<Int32>();
        }

        protected override void ExecuteInternal(
            IEnumerable<TsDataList> inputDataListList,
            TsDataList outputDataList)
        {
            var outputDataListT = outputDataList as TsDataListT<Int32>;
            var mer = new MersenneTwister();
            Action action = null;

            if (this.randomType == MdRandom.RandomType.None)
            {
                action = new Action(() => outputDataListT.AddValue(mer.Next()));
            }
            else if (this.randomType == MdRandom.RandomType.MinMax)
            {
                action = new Action(() => outputDataListT.AddValue(mer.Next(this.minValue, this.maxValue)));
            }
            else if (this.randomType == MdRandom.RandomType.ExternalMinMax)
            {
                Debug.Assert(inputDataListList.Count() == 2);

                // データ型の変換
                var convertedInputDataList0 = TsAppContext.AutoCastEnabled ?
                    inputDataListList.ElementAt(0).Convert(typeof(Int32)) : inputDataListList.ElementAt(0);
                var convertedInputDataList1 = TsAppContext.AutoCastEnabled ?
                    inputDataListList.ElementAt(1).Convert(typeof(Int32)) : inputDataListList.ElementAt(1);

                if (convertedInputDataList0.Count != 1)
                {
                    throw new TsInputDataInvalidLengthException(
                        1, this.DefinitionPath, 0);
                }
                else if (convertedInputDataList1.Count != 1)
                {
                    throw new TsInputDataInvalidLengthException(
                        1, this.DefinitionPath, 1);
                }

                var convertedInputDataListT0 = convertedInputDataList0 as TsDataListT<Int32>;
                var convertedInputDataListT1 = convertedInputDataList1 as TsDataListT<Int32>;

                var minValue = convertedInputDataListT0.GetValue(0);
                var maxValue = convertedInputDataListT1.GetValue(0);

                action = new Action(() => outputDataListT.AddValue(mer.Next(minValue, maxValue)));
            }
            
            for (var i = 0; i < this.length; i++)
            {
                action();
            }
        }
    }
}
