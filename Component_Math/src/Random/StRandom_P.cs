﻿using System;
using System.Diagnostics;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal class StRandom_P
        : DsEditForm_P<StRandom_VI, DsRandom>
    {
        public StRandom_P(DsRandom com)
            : base(com)
        {
        }

        public override void Load()
        {
            this.View.Initialize();
            this.View.RandomType = this.Component.RandomType;
            this.View.Length = this.Component.Length.ToString();
            this.View.MinValue = this.Component.MinValue.ToString();
            this.View.MaxValue = this.Component.MaxValue.ToString();
        }

        public override void Save()
        {
            this.Component.RandomType = this.View.RandomType;

            Int32 value;
            Boolean isSuccess;

            isSuccess = Int32.TryParse(this.View.Length, out value);
            Debug.Assert(isSuccess);
            this.Component.Length = value;
            
            isSuccess = Int32.TryParse(this.View.MinValue, out value);
            Debug.Assert(isSuccess);
            this.Component.MinValue = value;

            isSuccess = Int32.TryParse(this.View.MaxValue, out value);
            Debug.Assert(isSuccess);
            this.Component.MaxValue = value;
        }

        public override string Check()
        {
            this.View.ClearError();

            Boolean isSuccess;

            Int32 length;
            isSuccess = Int32.TryParse(this.View.Length, out length);
            if (!isSuccess)
            {
                this.View.SetLengthError(Resource.Random_Error_001);
                return Resource.Random_Error_001;
            }

            Int32 minValue;
            isSuccess = Int32.TryParse(this.View.MinValue, out minValue);
            if (!isSuccess)
            {
                this.View.SetMinValueError(Resource.Random_Error_001);
                return Resource.Random_Error_001;
            }

            Int32 maxValue;
            isSuccess = Int32.TryParse(this.View.MaxValue, out maxValue);
            if (!isSuccess)
            {
                this.View.SetMaxValueError(Resource.Random_Error_002);
                return Resource.Random_Error_001;
            }

            if (this.View.RandomType == MdRandom.RandomType.MinMax &&
                maxValue < minValue)
            {
                this.View.SetMinValueError(Resource.Random_Error_002);
                this.View.SetMaxValueError(Resource.Random_Error_002);
                return Resource.Random_Error_002;
            }

            return null;
        }
    }
}
