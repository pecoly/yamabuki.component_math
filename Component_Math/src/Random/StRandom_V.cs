﻿using System;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

using Yamabuki.Design.EditForm;

namespace Yamabuki.Component.Math
{
    internal partial class StRandom_V
        : DsEditForm_V, StRandom_VI
    {
        private EventHandler okayButton_Click;

        private EventHandler cancelButton_Click;

        private DataTable randomTypeTable = new DataTable();

        public StRandom_V()
        {
            this.InitializeComponent();
        }

        public String Message
        {
            set { this.statusLabel.Text = value; }
        }

        public String Length
        {
            get { return this.lengthText.Text; }
            set { this.lengthText.Text = value; }
        }

        public String MinValue
        {
            get { return this.minValueText.Text; }
            set { this.minValueText.Text = value; }
        }

        public String MaxValue
        {
            get { return this.maxValueText.Text; }
            set { this.maxValueText.Text = value; }
        }

        public MdRandom.RandomType RandomType
        {
            get { return (MdRandom.RandomType)this.randomTypeComboBox.SelectedValue; }
            set { this.randomTypeComboBox.SelectedValue = value; }
        }

        public Action OkButton_Click
        {
            set
            {
                this.okButton.Click -= this.okayButton_Click;
                this.okayButton_Click = (sender, e) => value();
                this.okButton.Click += this.okayButton_Click;
            }
        }

        public Action CancelButton_Click
        {
            set
            {
                this.cancelButton.Click -= this.cancelButton_Click;
                this.cancelButton_Click = (sender, e) => value();
                this.cancelButton.Click += this.cancelButton_Click;
            }
        }

        public override void Initialize()
        {
            this.InitializeRandomTypeComboBox();

            this.randomTypeComboBox.SelectedIndexChanged +=
                (sender, e) => this.UpdateText();

            this.UpdateText();
        }

        public void ClearError()
        {
            this.errorProvider.SetError(this.lengthText, null);
            this.errorProvider.SetError(this.minValueText, null);
        }

        public void SetLengthError(String value)
        {
            this.errorProvider.SetError(this.lengthText, value);
        }

        public void SetMinValueError(String value)
        {
            this.errorProvider.SetError(this.minValueText, value);
        }

        public void SetMaxValueError(String value)
        {
            this.errorProvider.SetError(this.maxValueText, value);
        }

        internal void UpdateText()
        {
            Boolean enabled = false;

            if (this.RandomType == MdRandom.RandomType.None)
            {
                enabled = false;
            }
            else if (this.RandomType == MdRandom.RandomType.MinMax)
            {
                enabled = true;
            }
            else if (this.RandomType == MdRandom.RandomType.ExternalMinMax)
            {
                enabled = false;
            }
            else
            {
                Debug.Assert(false);
            }

            this.minValueText.Enabled = enabled;
            this.maxValueText.Enabled = enabled;
        }

        private void InitializeRandomTypeComboBox()
        {
            this.randomTypeTable = this.CreateRandomTypeTable();

            this.randomTypeComboBox.DataSource = this.randomTypeTable;
            this.randomTypeComboBox.DisplayMember = RandomTypeTable.Name;
            this.randomTypeComboBox.ValueMember = RandomTypeTable.RandomType;
        }

        private DataTable CreateRandomTypeTable()
        {
            var randomTypeTable = new DataTable();

            randomTypeTable.Columns.Add(RandomTypeTable.Name, typeof(String));
            randomTypeTable.Columns.Add(RandomTypeTable.RandomType, typeof(MdRandom.RandomType));

            randomTypeTable.Rows.Add(new Object[] { "指定なし", MdRandom.RandomType.None });
            randomTypeTable.Rows.Add(new Object[] { "最小値・最大値を指定", MdRandom.RandomType.MinMax });
            randomTypeTable.Rows.Add(new Object[] { "最小値・最大値を外部入力で指定", MdRandom.RandomType.ExternalMinMax });

            return randomTypeTable;
        }

        public class RandomTypeTable
        {
            public const String Name = "Name";
            public const String RandomType = "RandomType";
        }
    }
}
