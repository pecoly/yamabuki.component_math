﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yamabuki.Component.Math
{
    public class MdRandom
    {
        public enum RandomType
        {
            /// <summary>指定なし</summary>
            None,

            /// <summary>最大値、最小値を指定</summary>
            MinMax,

            /// <summary>最大値、最小値を外部ポートで指定</summary>
            ExternalMinMax,
        }

        public static String ToString(RandomType type)
        {
            switch (type)
            {
                case RandomType.None:
                    return "None";
                case RandomType.MinMax:
                    return "MinMax";
                case RandomType.ExternalMinMax:
                    return "ExternalMinMax";
                default:
                    return "";
            }
        }

        public static RandomType ToRandomType(String value)
        {
            switch (value)
            {
                case "None":
                    return RandomType.None;
                case "MinMax":
                    return RandomType.MinMax;
                case "ExternalMinMax":
                    return RandomType.ExternalMinMax;
                default:
                    return RandomType.None;
            }
        }
    }
}
