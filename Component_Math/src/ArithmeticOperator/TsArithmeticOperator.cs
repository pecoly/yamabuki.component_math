﻿using System;
using System.Collections.Generic;

using Yamabuki.Component.Math.Properties;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.ExceptionEx;
using Yamabuki.Task.Function.Base;
using Yamabuki.Task.Function.Create;
using Yamabuki.Utility.Cast;

namespace Yamabuki.Component.Math
{
    /// <summary>
    /// 四則演算の基底クラス
    /// 入力2つに出力が1つです。
    /// 1つ目の入力データの型と2つ目の入力データの型は任意です。
    /// 出力のデータ型は2つの入力データの型から決定します。
    /// </summary>
    public abstract class TsArithmeticOperator
        : TsTask_2_1
    {
        private TsFunction_2_1 func;
        
        private TsCreateFunction createFunc;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="definitionPath">定義名称パス</param>
        /// <param name="inputDataStoreGuid0">入力データストア0</param>
        /// <param name="inputDataStoreGuid1">入力データストア1</param>
        /// <param name="outputDataStoreGuid">出力データストア</param>
        /// <param name="createFunc">データリスト作成関数</param>
        /// <param name="func">演算関数</param>
        public TsArithmeticOperator(
            String definitionPath,
            TsDataStoreGuid inputDataStoreGuid0,
            TsDataStoreGuid inputDataStoreGuid1,
            TsDataStoreGuid outputDataStoreGuid,
            TsCreateFunction createFunc,
            TsFunction_2_1 func)
            : base(definitionPath, inputDataStoreGuid0, inputDataStoreGuid1, outputDataStoreGuid)
        {
            this.createFunc = createFunc;
            this.func = func;
        }
                
        protected override void CheckInputDataList(
            TsDataList inputDataList0,
            TsDataList inputDataList1)
        {
            try
            {
                this.ConvertType(inputDataList0, inputDataList1);
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInvalidDataType:
                        throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }

        protected Tuple<Type, Type, Type> ConvertType(
            TsDataList inputDataList0,
            TsDataList inputDataList1)
        {
            if (TsAppContext.AutoCastEnabled)
            {
                var int32int32 = inputDataList0.Type == typeof(Int32) && inputDataList1.Type == typeof(Int32);
                var int32single = inputDataList0.Type == typeof(Int32) && inputDataList1.Type == typeof(Single);
                var singleint32 = inputDataList0.Type == typeof(Single) && inputDataList1.Type == typeof(Int32);
                var singlesingle = inputDataList0.Type == typeof(Single) && inputDataList1.Type == typeof(Single);

                if (int32int32 || int32single || singleint32 || singlesingle)
                {
                    return new Tuple<Type, Type, Type>(
                        inputDataList0.Type,
                        inputDataList1.Type,
                        this.func.GetResultType(inputDataList0.Type, inputDataList1.Type));
                }

                if (inputDataList0.Type == typeof(Int32))
                {
                    return new Tuple<Type, Type, Type>(
                        inputDataList0.Type,
                        typeof(Int32),
                        this.func.GetResultType(inputDataList0.Type, typeof(Int32)));
                }
                else if (inputDataList0.Type == typeof(Single))
                {
                    return new Tuple<Type, Type, Type>(
                        inputDataList0.Type,
                        typeof(Single),
                        this.func.GetResultType(inputDataList0.Type, typeof(Single)));
                }
                else if (inputDataList1.Type == typeof(Int32))
                {
                    return new Tuple<Type, Type, Type>(
                        typeof(Int32),
                        inputDataList1.Type,
                        this.func.GetResultType(typeof(Int32), inputDataList1.Type));
                }
                else if (inputDataList1.Type == typeof(Single))
                {
                    return new Tuple<Type, Type, Type>(
                        typeof(Single),
                        inputDataList1.Type,
                        this.func.GetResultType(typeof(Int32), inputDataList1.Type));
                }

                return new Tuple<Type, Type, Type>(
                    typeof(Single),
                    typeof(Single),
                    this.func.GetResultType(typeof(Single), typeof(Single)));
            }
            else
            {
                return new Tuple<Type, Type, Type>(
                    inputDataList0.Type,
                    inputDataList1.Type,
                    this.func.GetResultType(inputDataList0.Type, inputDataList1.Type));
            }
        }

        protected override void CreateOutputDataList(out TsDataList outputDataList)
        {
            var inputDataStore0 = this.GetInputDataStore(Constants.InputPortIndex0);
            var inputDataStore1 = this.GetInputDataStore(Constants.InputPortIndex1);

            var typeList = this.ConvertType(inputDataStore0.DataList, inputDataStore1.DataList);
            outputDataList = this.createFunc.Execute(typeList.Item3);
        }

        protected override void ExecuteInternal(
            TsDataList inputDataList0,
            TsDataList inputDataList1,
            TsDataList outputDataList)
        {
            var typeList = this.ConvertType(inputDataList0, inputDataList1);

            // データ型の変換
            var convertedInputDataList0 = TsAppContext.AutoCastEnabled ?
                inputDataList0.Convert(typeList.Item1) : inputDataList0;
            var convertedInputDataList1 = TsAppContext.AutoCastEnabled ?
                inputDataList1.Convert(typeList.Item2) : inputDataList1;

            try
            {
                this.func.Execute(convertedInputDataList0, convertedInputDataList1, outputDataList);
            }
            catch (TsFunctionException e)
            {
                switch (e.ErrorCode)
                {
                    case TsErrorCodeList.TsInputDataInvalidLength:
                        throw new TsInputDataInvalidLengthException(this.DefinitionPath, Resource.InvalidDataLength);
                    case TsErrorCodeList.TsInvalidDataType:
                        throw new TsInvalidDataTypeException(this.DefinitionPath, Resource.UnsupportedDataType);
                    default:
                        throw new TsException(this.DefinitionPath, e.ErrorCode, Resource.UndefinedException);
                }
            }
        }
    }
}
