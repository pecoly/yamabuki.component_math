﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.MathTest
{
    [TestFixture]
    public class DsMultiplyTest_004
        : MathBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsSingle
        ///  DsInteger
        ///  DsMultiply
        ///  DsListView
        ///  
        /// DsSingle ->
        ///              DsMultiply -> DsListView
        /// DsInteger ->
        /// </summary>
        [Test]
        public void Execute()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var s0 = this.CreateDsFloatMock("s0", r);
            var i1 = this.CreateDsIntegerMock("i1", r);
            var m = this.CreateDsMultiply("m", r);
            var v = this.CreateDsTerminatorMock("v", r);

            DsComponentConnectionUtils.ConnectForData(
                s0.OutputPortCollection.GetPort(0),
                m.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                i1.OutputPortCollection.GetPort(0),
                m.InputPortCollection.GetPort(1));
            DsComponentConnectionUtils.ConnectForData(
                m.OutputPortCollection.GetPort(0),
                v.InputPortCollection.GetPort(0));

            //  Xml出力
            {
                var x0 = s0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "0");

                s0.Value = -9.5F;
                x0 = s0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "-9.5");
            }

            {
                var x1 = i1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "0");

                i1.Value = 11;
                x1 = i1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "11");
            }

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(v.Result, "-104.5\r\n");
        }
    }
}
