﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using NUnit.Framework;

using ComponentFactory.Krypton.Toolkit;

using Yamabuki.Component.Math;
using Yamabuki.Test.View;

namespace Yamabuki.Component.MathTest
{
    [TestFixture]
    public class StAggregateOperator_VTest
    {
        private StAggregateOperator_V_N_1 view = new StAggregateOperator_V_N_1();

        [Test]
        public void StartPosition()
        {
            Assert.AreEqual(view.StartPosition, FormStartPosition.CenterScreen);
        }

        [Test]
        public void BorderStyle()
        {
            Assert.AreEqual(view.FormBorderStyle, FormBorderStyle.FixedSingle);
        }

        [Test]
        public void MaximizeBox()
        {
            Assert.AreEqual(view.MaximizeBox, true);
        }

        [Test]
        public void Control()
        {
            ViewTest.Control(view.Controls);
        }

        [Test]
        public void KryptonButton()
        {
            ViewTest.KryptonButton(view.Controls);
        }

    }
}
