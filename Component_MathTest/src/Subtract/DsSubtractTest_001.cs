﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.MathTest
{
    [TestFixture]
    public class DsSubtractTest_001
        : MathBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsInteger
        ///  DsInteger
        ///  DsSubtract
        ///  DsListView
        ///  
        /// DsInteger ->
        ///              DsSubtract -> DsListView
        /// DsInteger ->
        /// </summary>
        [Test]
        public void Execute()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var i0 = this.CreateDsIntegerMock("i0", r);
            var i1 = this.CreateDsIntegerMock("i1", r);
            var s = this.CreateDsSubtract("s", r);
            var v = this.CreateDsTerminatorMock("v", r);

            DsComponentConnectionUtils.ConnectForData(
                i0.OutputPortCollection.GetPort(0),
                s.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                i1.OutputPortCollection.GetPort(0),
                s.InputPortCollection.GetPort(1));
            DsComponentConnectionUtils.ConnectForData(
                s.OutputPortCollection.GetPort(0),
                v.InputPortCollection.GetPort(0));

            //  Xml出力
            {
                var x0 = i0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "0");

                i0.Value = 100;
                x0 = i0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "100");
            }

            {
                var x1 = i1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "0");

                i1.Value = 123;
                x1 = i1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "123");
            }

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(v.Result, "-23\r\n");
        }
    }
}
