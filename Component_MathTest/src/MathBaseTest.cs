﻿using System;
using System.IO;
using System.Xml.Linq;

using Yamabuki.Component.Math;
using Yamabuki.Design;
using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Component.RootSystem;
using Yamabuki.Design.Container;
using Yamabuki.Design.Connection;
using Yamabuki.Design.Utility;
using Yamabuki.Test;

namespace Yamabuki.Component.MathTest
{
    public class MathBaseTest
        : BaseTest
    {
        String nameSpace = "Yamabuki.Component.MathTest";
        
        public override void Initialize()
        {
            base.Initialize();

            String project = "Component_MathTest";
            String name = "yamabuki.component_math";
            String target = "yamabuki";
            String bin = "bin\\Debug";
            Int32 pos;

            DsAppContext app = new DsAppContextMock();
            DsAppContext.SetInstance(app);

            String path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            pos = path.IndexOf(project);
            if (pos != -1)
            {
                bin = path.Substring(pos + project.Length + 1);
            }

            pos = path.IndexOf(target, StringComparison.OrdinalIgnoreCase);
            if (pos == -1)
            {
                throw new DirectoryNotFoundException(path);
            }

            this.SolutionPath = path.Substring(0, pos + target.Length) + "\\" + name;
            this.ProjectPath = this.SolutionPath + "\\" + project;
            this.ExePath = Path.Combine(this.ProjectPath, bin);
        }
        
        public DsAdd CreateDsAdd(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsAdd>(this.Collection, 0, 0, name, parent);
        }

        public DsSubtract CreateDsSubtract(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsSubtract>(this.Collection, 0, 0, name, parent);
        }

        public DsMultiply CreateDsMultiply(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsMultiply>(this.Collection, 0, 0, name, parent);
        }

        public DsDivide CreateDsDivide(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsDivide>(this.Collection, 0, 0, name, parent);
        }

        public override String ExePath { get; set; }
        public String SolutionPath { get; private set; }
        public String ProjectPath { get; private set; }
    }
}
