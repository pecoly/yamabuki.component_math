﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.MathTest
{
    [TestFixture]
    public class DsAddTest_003
        : MathBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsInteger
        ///  DsSingle
        ///  DsAdd
        ///  DsListView
        ///  
        /// DsInteger ->
        ///              DsAdd -> DsTerminator
        /// DsSingle ->
        /// </summary>
        [Test]
        public void Execute()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var i0 = this.CreateDsIntegerMock("i0", r);
            var s1 = this.CreateDsFloatMock("s1", r);
            var a = this.CreateDsAdd("a", r);
            var v = this.CreateDsTerminatorMock("v", r);

            DsComponentConnectionUtils.ConnectForData(
                i0.OutputPortCollection.GetPort(0),
                a.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                s1.OutputPortCollection.GetPort(0),
                a.InputPortCollection.GetPort(1));
            DsComponentConnectionUtils.ConnectForData(
                a.OutputPortCollection.GetPort(0),
                v.InputPortCollection.GetPort(0));

            //  Xml出力
            {
                var x0 = i0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "0");

                i0.Value = 100;
                x0 = i0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "100");
            }

            {
                var x1 = s1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "0");

                s1.Value = 1.23F;
                x1 = s1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "1.23");
            }

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(v.Result, "101.23\r\n");
        }
    }
}
