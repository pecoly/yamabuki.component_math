﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.MathTest
{
    [TestFixture]
    public class DsDivideTest_002
        : MathBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsSingle
        ///  DsSingle
        ///  DsDivide
        ///  DsListView
        ///  
        /// DsSingle ->
        ///              DsDivide -> DsListView
        /// DsSingle ->
        /// </summary>
        [Test]
        public void Execute()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var s0 = this.CreateDsFloatMock("s0", r);
            var s1 = this.CreateDsFloatMock("s1", r);
            var d = this.CreateDsDivide("d", r);
            var v = this.CreateDsTerminatorMock("v", r);

            DsComponentConnectionUtils.ConnectForData(
                s0.OutputPortCollection.GetPort(0),
                d.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                s1.OutputPortCollection.GetPort(0),
                d.InputPortCollection.GetPort(1));
            DsComponentConnectionUtils.ConnectForData(
                d.OutputPortCollection.GetPort(0),
                v.InputPortCollection.GetPort(0));

            //  Xml出力
            {
                var x0 = s0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "0");

                s0.Value = 1.23F;
                x0 = s0.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x0.Element("Data"), "Value"), "1.23");
            }

            {
                var x1 = s1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "0");

                s1.Value = 4.92F;
                x1 = s1.GetXElement();
                Assert.AreEqual(XmlUtils.GetElementValue(x1.Element("Data"), "Value"), "4.92");
            }

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(v.Result, "0.25\r\n");
        }
    }
}
