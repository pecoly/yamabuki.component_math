﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yamabuki.Component.MathTest
{
    public class Program
    {
        static void DsAdd_001()
        {
            var t = new DsAddTest_001();
            t.Initialize();
            t.Execute();
        }

        static void DsAdd_002()
        {
            var t = new DsAddTest_002();
            t.Initialize();
            t.Execute();
        }

        static void DsAdd_003()
        {
            var t = new DsAddTest_003();
            t.Initialize();
            t.Execute();
        }

        static void DsAdd_004()
        {
            var t = new DsAddTest_004();
            t.Initialize();
            t.Execute();
        }

        static void DsSubtract_001()
        {
            var t = new DsSubtractTest_001();
            t.Initialize();
            t.Execute();
        }

        static void DsSubtract_002()
        {
            var t = new DsSubtractTest_002();
            t.Initialize();
            t.Execute();
        }

        static void DsSubtract_003()
        {
            var t = new DsSubtractTest_003();
            t.Initialize();
            t.Execute();
        }

        static void DsSubtract_004()
        {
            var t = new DsSubtractTest_004();
            t.Initialize();
            t.Execute();
        }

        static void DsMultiply_001()
        {
            var t = new DsMultiplyTest_001();
            t.Initialize();
            t.Execute();
        }

        static void DsMultiply_002()
        {
            var t = new DsMultiplyTest_002();
            t.Initialize();
            t.Execute();
        }

        static void DsMultiply_003()
        {
            var t = new DsMultiplyTest_003();
            t.Initialize();
            t.Execute();
        }

        static void DsMultiply_004()
        {
            var t = new DsMultiplyTest_004();
            t.Initialize();
            t.Execute();
        }

        static void DsDivide_001()
        {
            var t = new DsDivideTest_001();
            t.Initialize();
            t.Execute();
        }

        static void DsDivide_002()
        {
            var t = new DsDivideTest_002();
            t.Initialize();
            t.Execute();
        }

        static void DsDivide_003()
        {
            var t = new DsDivideTest_003();
            t.Initialize();
            t.Execute();
        }

        static void DsDivide_004()
        {
            var t = new DsDivideTest_004();
            t.Initialize();
            t.Execute();
        }
        
        static void Main(string[] args)
        {
            try
            {
                DsAdd_001();
                DsAdd_002();
                DsAdd_003();
                DsAdd_004();

                DsSubtract_001();
                DsSubtract_002();
                DsSubtract_003();
                DsSubtract_004();

                DsMultiply_001();
                DsMultiply_002();
                DsMultiply_003();
                DsMultiply_004();

                DsDivide_001();
                DsDivide_002();
                DsDivide_003();
                DsDivide_004();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
